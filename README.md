Gateway
===================

Microservice testing environment. While write a microservice is easy, the difficult increases when it must be tested alongside the other microservices in the ecosystem. This tool is intended to be used as an integration testing platform.

Microservices are usually deployed on Container-deployer applications like Kubernetes, Nomad or Mesos. In some cases, the computational requirements of these platforms can slow down the whole computer. Then, it use for testing purposes is limited. Another option would be the use of docker-compose. However, it lacks some features of the previous platforms, such as: service discovery, secret management or api gateway.

The following tool introduces docker-compose compatible format in HCL (Hashicorp Configuration Language) which can be used to deploy services as would be done on docker-compose. Besides, it deploys alternative services to deal with the service discovery (Consul), secret management (Vault) and api gateway (Kong).

Internally, the tool takes a deployment.hcl and creates a docker-compose.yml file with the consul, vault and kong services. It then relies on docker-compose to deploy the services.

----------


Format
-------------

When running the command line, the file deployment.hcl on the local folder represents the configuration of the deployment (i.e. docker-compose.yml). Two services from local folders
```
service "service1" {
	src = ".."
}

service "service2" {
	src = "..."
}
```

On runtime, there will be two entries on the consul DNS one for each service given the open ports of each one. Like the format is compatible with docker-compose, it is possible to:

```
service "service1" {
	image = "imagename"
	ports = ["80:80"]
}
```

On the future, there will be steps to configure internally the properties of consul and vault.
Another important requirement for microservices is the possibility to introduce an API gateway to route http requests given the path. This is possible through Kong and is specified on the deployment.hcl as:
```
gateway {
	// TODO
}
```
----------


Command Line
-------------

When running the command, it will search for the file deployment.hcl on the local folder. Right now, it does not allow to load the file from another location. It will create as well another file spec.hcl with the internal result of the deployment.

#### Build

It will create temporary images for the services that use local folders.
```
$ gateway build
```
#### Destroy

Remove the whole deployment
```
$ gateway destroy
```
#### Deploy

Deploy the application with docker-compose.
```
$ gateway deploy
```
