package cmd

import (
	"gateway/pkg"
	"io/ioutil"
	"os"

	"path/filepath"

	"github.com/hashicorp/hcl"
	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(DeployCmd)
	RootCmd.AddCommand(DestroyCmd)
}

const (
	descFile = "deployment.hcl"
	specFile = "spec.hcl"
)

var RootCmd = &cobra.Command{
	Use:   "micro",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}

func existsDescFile() bool {
	return existsFile(descFile)
}

func getDeploymentFromDesc() (*pkg.Deployment, error) {
	return getDeploymentFromFile(descFile)
}

func getDeploymentFromFile(path string) (*pkg.Deployment, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var d *pkg.Deployment
	err = hcl.Unmarshal([]byte(data), &d)
	if err != nil {
		return nil, err
	}

	return d, nil
}

func existsFile(path string) bool {
	if _, err := os.Stat(filepath.Join(".", path)); !os.IsNotExist(err) {
		return true
	}

	return false
}
