package cmd

import (
	"fmt"
	"io/ioutil"

	gateway "gateway/pkg"

	"github.com/spf13/cobra"
)

var DeployCmd = &cobra.Command{
	Use:   "deploy",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		err := deploy()
		if err != nil {
			panic(err)
		}
	},
}

func deploy() error {

	if !existsDescFile() {
		return fmt.Errorf("NO EXISTE DESC FILE")
	}

	desc, err := getDeploymentFromDesc()
	if err != nil {
		return err
	}
	desc.CompleteServices()

	desc.AddService("registrator", gateway.RegistratorSvc)
	desc.AddService("consul", gateway.ConsulSvc)
	
	data, err := desc.ToYaml()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile("./docker-compose.yml", data, 0644)
	if err != nil {
		return err
	}

	return nil
}
