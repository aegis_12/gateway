package pkg

type Service struct {
	Name      string   `yaml:"-"`
	ID        string   `yaml:"-"`
	Image     string   `yaml:"image"`
	Src       string   `yaml:"-" hcl:"src"`
	Hostname  string   `yaml:"hostname,omitempty"`
	Command   string   `yaml:"command,omitempty"`
	Ports     []string `yaml:"ports,omitempty"`
	DNS       []string `yaml:"dns,omitempty"`
	DNSearch  []string `yaml:"dns_search,omitempty"`
	Volumes   []string `yaml:"volumes,omitempty"`
	Net       string   `yaml:"network_mode,omitempty"`
	DependsOn []string `yaml:"depends_on,omitempty"`
}

func (s *Service) completeService() {
	s.DNS = []string{
		"172.17.0.1",
		"8.8.8.8",
	}
	s.DNSearch = []string{
		"service.consul",
	}
}

func (s *Service) Build() (bool, error) {
	if !s.requiresBuild() {
		return false, nil
	}

	// si el imagename es cero es proque no existe no?
	// segun si viene de spec o de deployment

	return false, nil
}

// intentar poner dentro de servicio el nombre tambien
func (s *Service) ImageName() string {
	return "" // + s.Name
}

func (s *Service) requiresBuild() bool {
	return false
}
