package pkg

import yaml "gopkg.in/yaml.v2"

type Deployment struct {
	Version  string
	Services map[string]*Service `hcl:"service"`
}

func (d *Deployment) CompleteServices() {
	d.Version = "2.0"

	for name, i := range d.Services {
		i.Name = name
		i.completeService()
	}
}

func (d *Deployment) ToYaml() ([]byte, error) {
	return yaml.Marshal(&d)
}

func (d *Deployment) AddService(name string, s *Service) {
	d.Services[name] = s
}
