package pkg

var ConsulSvc = &Service{
	Image: "gliderlabs/consul-server",
	Ports: []string{
		"8400:8400",
		"8500:8500",
		"172.17.0.1:53:8600/udp",
	},
	Command: "-node myconsul -bootstrap",
}

var RegistratorSvc = &Service{
	Image: "gliderlabs/registrator",
	Volumes: []string{
		"/var/run/docker.sock:/tmp/docker.sock",
	},
	Command: "consul://localhost:8500",
	Net:     "host",
	DependsOn: []string{
		"consul",
	},
}

/*

docker run -d --name=registrator --net=host --volume=/var/run/docker.sock:/tmp/docker.sock gliderlabs/registrator:latest consul://localhost:8500

docker run -d -p 8500:8500 -p 172.17.0.1:53:8600/udp -p 8400:8400 gliderlabs/consul-server -node myconsul -bootstrap

docker run -d --dns=172.17.0.1 --dns=8.8.8.8 --dns-search=service.consul -P --name=nginx2 nginx

*/
